FROM nginx

RUN apt-get update && apt-get -y install zip

COPY 02_Continuous_Delivery/html /usr/share/nginx/html
RUN zip -r /usr/share/nginx/html/site.zip /usr/share/nginx/html
EXPOSE 80
#CMD ["nginx","-g","daemon off;"]
